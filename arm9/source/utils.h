#include <nds.h>
#include <dirent.h>
#include <stdio.h>
#include <cstdarg>

#include "menu.h"
#include "flashing.h"
#include "bmp15.h"

u32 memViewTools(u32 address);
int fastVerify(FILE *fp);
int checkBlock(FILE *fp, u16 sector);
int askMulticartOffset();
std::string getFile(const char* title, std::string &dir);
void printTop(const char* str, ...);
void detectionTest();
void initGraphics();
void verify();
void updateUI();
void startFlashingUI();
void stopFlashingUI();
void memView();
void listMulticartGames();
bool isGame();
bool checkSramBankSwitching();
bool isHeaderMirroredAt(u32 addr);


void BootGbaARM9(u32 useBottomScreen, cBMP15& border);
int dirExists(const char* path);
int fileExists(char* name);
