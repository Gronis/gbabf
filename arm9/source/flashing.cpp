#include "flashing.h"

const int BUFFER_SIZE = 0x10000;
u8 gFlippedDataBits;
u32 gSectorSize;
char gDatabuf[BUFFER_SIZE];
rombuffer gRomBuf;
opStatus gStatus;


u8 getFlipDataBitsFlag(cartType cartType){
	return (cartType.flags & CART_TYPE_FLIP_DATA_BITS) == CART_TYPE_FLIP_DATA_BITS;
}

u8 getWaitForUnlockFlag(cartType cartType){
	return (cartType.flags & CART_TYPE_IGNORE_WAIT_FOR_UNLOCK) != CART_TYPE_IGNORE_WAIT_FOR_UNLOCK;
}

u32 getFileSize(FILE* fp){
	fseek(fp, 0L, SEEK_END);
	u32 size = ftell(fp);
	rewind(fp);
	return size;
}


rombuffer* getBuffer(){
	gRomBuf.data = gDatabuf;
	gRomBuf.size = 0;
	return &gRomBuf;
}



rombuffer* fillBuffer(rombuffer* buf, u32 size, FILE *fp){
	size_t bytesRead = fread(buf->data, sizeof(u8), size, fp);
	buf->size = bytesRead;
	if(bytesRead < size){
		buf->data[bytesRead] = 0xFF;
	}
	return buf;
}


vu16 readFlash(u32 addr){
	return *(vu16*)(addr|0x8000000);
}

vu8 readFlashByte(u32 addr){
	return (vu8)(readFlash(addr) & 0xFF);
}


volatile void writeFlash(u32 addr, u16 data){
	*(vu16 *)(addr|0x8000000) = data;
	asm("nop");
}


void writeFlashFlip(u32 addr, u16 data){
	if(gFlippedDataBits){
		u16 flipped = data & 0xFFFC;
		flipped |= ( data & 1 ) << 1;
		flipped |= ( data & 2 ) >> 1;
		writeFlash(addr, flipped);
	}else writeFlash(addr, data);
}

void writeArray(u32 addr, u8 len, u16 data[]){
	for(int i = 0; i<len;i++){
		writeFlash(addr,data[i]);
	}
}


bool waitForFlash(u32 address, bool (*isReady)(u16), int timeout){
	while(timeout && !isReady(readFlash(address)) ){
		timeout--;
	}
	if(!timeout){
		return false;
	}
	return true;
}


void printWriteErrorMsg(u32 addr, u16 expected){
	printTop("Write failed at 0x%04X.\nAborting...\n", addr);
	printTop("Expected data: 0x%02X\n", expected);
	printTop("Data at address: 0x%02X\n", readFlash(addr));
}




cartType detectIntel(){
	u16 id = readFlash(0x2);
	u16 bufSigns[] = {0x8902, 0x8904, 0x887D, 0x88B0};
	u16 nonbufSigns[] = {0x88C6, 0x88C4, 0x880D, 0x8813, 0x880C, 0x880F, 0x8812, 0x8816, 0x8815,
	       	0x8810, 0x880E, 0x8855, 0x8857};
	
	
	for(unsigned int i = 0; i < (sizeof(nonbufSigns)/sizeof(u16));i++){
		if(id == nonbufSigns[i])
			return {INTEL, 0};
	}
	
	for(unsigned int i = 0;i < (sizeof(bufSigns)/sizeof(u16));i++){
		if(id == bufSigns[i])
			return {INTEL_BUFFERED,0};
	}
	
	if(id == 0x887E){
		return {INTEL_BUFFERED, CART_TYPE_FLIP_DATA_BITS};
	}
	
	if(id == 0xB0 && readFlash(0) == 0xB0){
		return {INTEL, 0};
	}

	if(readFlashByte(0) == 0x1C && readFlashByte(1) == 0x00 && readFlashByte(2) == 0x2A && readFlashByte(3) == 0x00){
		return {INTEL, CART_TYPE_IGNORE_WAIT_FOR_UNLOCK};
	}

	return { UNKNOWN, 0};
}


cartType detect(){
	u16 initialData = readFlash(0x0);
	
	//INTEL
	u16 intelDetect[3] = {0xFF, 0x50, 0x90};
	writeArray(0x0, 3, intelDetect);
	
	if(readFlash(0) != initialData){
		cartType intelType = detectIntel();
		writeFlash(0x0, 0xFF);
		switch(intelType.method){
			case UNKNOWN:
			break;

			default:
			return intelType;
		}
	}
	
	
	//MSP100
	writeFlash(0x154, 0xF0F0);
	writeFlash(0x154, 0x9898);
	if(readFlash(0x40) == 0x5152 && readFlash(0x44) == 0x5251){
		writeFlash(0x154, 0xF0F0);
		return {MSP100, 0};
	}
	
	
	//MSP128
	resetMSP128();
	writeFlashFlip(0xAAA, 0xA9);
	writeFlash(0x554, 0x56);
	writeFlash(0xAAA, 0x90);
	
	if(readFlash(2) == 0x227D){
		resetMSP128();
		return {MSP128, 0};
	}else if(readFlash(2) == 0x227E){
		resetMSP128();
		return {MSP128, CART_TYPE_FLIP_DATA_BITS};
	}
	
	
	//MSP100SHIFTED
	writeFlash(0x0, 0xF0F0);
	writeFlash(0xAA, 0x9898);
	if(readFlash(0x20) == 0x5152){
		writeFlash(0x0, 0xF0F0);
		return {MSP100SHIFTED, 0};
	}
	
	
	//UNKNOWN
	writeFlash(0x0, 0xFF);
	writeFlash(0x0, 0xF0);
	writeFlash(0x0, 0xF0F0);
	writeFlash(0x154, 0xF0F0);
	return {UNKNOWN, 0}; 
}






int eraseFlashRom(cartType type){
	int (*func)(int);
	
	gFlippedDataBits = getFlipDataBitsFlag(type);
	switch(type.method){
		case INTEL:
			func = eraseSectorIntel;
		break;
		
		case INTEL_BUFFERED:
			func = eraseSectorIntel;
		break;
		
		case MSP100:
			func = eraseSectorMSP100;
		break;
		
		case MSP128:
			func = eraseSectorMSP128;
		break;
		
		case MSP100SHIFTED:
			func = eraseSectorMSP100Shifted;
		break;
		
		default:
			printTop("Unknown cart.\n");
		return -1;
	}
	
	gStatus.totalSectors = 512;
	
	for(int i = 0;i<512;i++){
		if(type.method == INTEL || type.method == INTEL_BUFFERED){
			if(unlockSectorIntel(i) == -1){
				printTop("Unlock failed on sector %d\n", i);
				return -1;
			}
		}
		if(func(i) == -1){
			printTop("Erase failed on sector %d\n", i);
			return -1;
		}
		gStatus.sectorsErased = i;
	}
	
	gFlippedDataBits = 0;
	return 0;
}



int writeFlashRom(FILE* fp, cartType type, u32 startAddr){
	int result;
	gFlippedDataBits = getFlipDataBitsFlag(type);
	switch(type.method){
		
		case INTEL:
			result = flashIntel(fp, startAddr, type);
		break;
		
		case INTEL_BUFFERED:
			result = flashIntelBuffered(fp, startAddr);
		break;
		
		case MSP100:
			result = flashMSP100(fp, startAddr);
		break;
		
		case MSP128:
			result = flashMSP128(fp, startAddr);
		break;
		
		case MSP100SHIFTED:
			result = flashMSP100Shifted(fp, startAddr);
		break;
		
		default:
			printTop("Unable to detect cart.\n");
		return -1;
	}
	gFlippedDataBits = 0;
	return result;
}




int unlockSectorIntel(int sector){
	u32 addr = sector*0x10000;
	u16 cmd[] = {0x50,0xFF,0x60,0xD0};
	writeArray(addr, 4, cmd);
	
	if(!waitForFlash(addr, WAIT_STATUS_READY, 0x100000)){
		writeFlash(addr, 0xFF);
		return -1;
	}

	writeFlash(addr, 0xFF);
	return 0;
}


int eraseSectorIntel(int sector){
	u32 addr = sector*0x10000;
	u16 cmd[] = {0x50,0xFF,0x20,0xD0};
	writeArray(addr, 4, cmd);
	
	waitForFlash(addr, WAIT_FOR_0, 0x10000);
	
	if(waitForFlash(addr, WAIT_STATUS_READY, TIMEOUT_8S)){
		writeFlash(addr, 0xFF);
		return 0;
	}

	if(readFlash(addr)&0x20){
			printTop("Sector %d erase failed\n", sector);
			writeFlash(addr, 0xFF);
			return -1;
	}
	
	printTop("Intel erase timeout on sector %d\n",sector);
	writeFlash(addr, 0xFF);
	return -1;
}




int writeWordIntel(u32 addr, u16 data){
	
	int tries = 0;
	retry:
	
	writeFlash(addr, 0x50);
	writeFlash(addr, 0xFF);
	
	writeFlash(addr, 0x40);
	writeFlash(addr, data);
	int timeout = 0x1000;
	

	
	while(timeout && readFlash(addr) == 0xFFFF){timeout--;}
	while(timeout && readFlash(addr) == 0){timeout--;}
	while(timeout && !(readFlash(addr)&0x80)){
		timeout--;
	}
	
	//Sector was locked, unlock & erase, then try again
	if(readFlash(addr) & 3 && !tries){
		printTop("Retrying erase at %02X\n", addr);
		u16 cmd[] = {0x50, 0xFF,0x60,0xD0,0xFF,0x20,0xD0};
		writeArray(addr, 7, cmd);
		tries++;
		waitForFlash(addr, WAIT_NON_FFFF, 0x1000);
		waitForFlash(addr, WAIT_STATUS_READY, TIMEOUT_5S);
		goto retry;
	}

	if(!timeout || (readFlash(addr) & 0x10)){
		writeFlash(addr, 0xFF);
		printWriteErrorMsg(addr, data);
		return -1;
	}
	
	writeFlash(addr, 0xFF);
	
	return 0;
}





int flashIntel(FILE *rom, u32 startAddr, cartType type){
	u32 fsz = getFileSize(rom);
	int sec = 1+(fsz-1)/0x10000;
	gStatus.totalSectors = sec;
	for(u16 i = startAddr/0x10000;i<(startAddr/0x10000)+sec;i++){
		if(unlockSectorIntel(i) < 0 && getWaitForUnlockFlag(type) ){
			printTop("Unable to unlock sector %d\n",i);
			return -1;
		}
		if(eraseSectorIntel(i) < 0 && getWaitForUnlockFlag(type)){
			printTop("Erase timed out on sector %d.\n", i);
			return -1;
		}
		gStatus.sectorsErased++;
	}
	rombuffer* rombuf = getBuffer();
	u32 bufAddr = 0;
	for(u32 i = startAddr;i<fsz;i+=2){
		if((i&(BUFFER_SIZE-1)) == 0){
			gStatus.wordsFlashed = i;
			
			fillBuffer(rombuf, BUFFER_SIZE, rom);
			bufAddr = 0;
		}
		u16 data = (rombuf->data[bufAddr] | (rombuf->data[bufAddr+1] << 8));
		if(writeWordIntel(i, data)==-1){
			writeFlash(0x0, 0xFF);
			return -1;
		}
		bufAddr+=2;
	}
	writeFlash(0x0, 0xFF);
	return 0;
}



int flashIntelBuffered(FILE* f, u32 startAddr){
	int fails = 0;
	int fsz = getFileSize(f);
	int sec = 1+(fsz-1)/0x10000;
	gStatus.totalSectors = sec;
	for(u16 i = (startAddr/0x10000);i<(startAddr/0x10000)+sec;i++){
		
		if(unlockSectorIntel(i) < 0){
			printTop("Unable to unlock sector %d\n",i);
			return -1;
		}
		if(eraseSectorIntel(i) < 0){
			printTop("Erase timed out on sector %d.\nAborting...\n", i);
			return -1;
		}
		gStatus.sectorsErased++;
	}
	rombuffer* rombuf = getBuffer();
	u32 writeAddr = startAddr;
	int bufAddr = 0;
	bool writing = true;

	while(writing){
	
	if(writeAddr % BUFFER_SIZE==0){
		gStatus.wordsFlashed = writeAddr;
		
		bufAddr=0;
		fillBuffer(rombuf, BUFFER_SIZE, f);
		if(rombuf->size <= 0x400){writing = false;}
		if(!rombuf->size){break;}
	}
	
	writeFlash(writeAddr, 0x50);
	writeFlash(writeAddr, 0xFF);
	writeFlashFlip(writeAddr, 0xEA);
	writeFlash(writeAddr, 0x1FF);

	for(int i=0;i<0x200;i++){
		u16 data = (rombuf->data[bufAddr] | (rombuf->data[bufAddr+1] << 8));
		writeFlash(writeAddr+(i*2), data);
		bufAddr+=2;
	}
	writeFlash(writeAddr, 0xD0);
	

	waitForFlash(writeAddr, WAIT_NON_FFFF, 0x1000);
	if(!waitForFlash(writeAddr, WAIT_STATUS_READY, 0x5000)){
		printTop("Writing timed out at 0x%06X\n", writeAddr);
		return -1;
	}
		
		//Sector was locked, unlock & erase, then try again
		if(readFlash(writeAddr)&3 && !fails){
			printTop("Retrying erase at %02X\n", writeAddr);
			u16 cmd[] = {0x50, 0xFF, 0x60, 0xD0, 0xFF, 0x20, 0xD0};
			writeArray(writeAddr, 7, cmd);
			waitForFlash(writeAddr, WAIT_FOR_0, 0x1000);
			if(!waitForFlash(writeAddr, WAIT_STATUS_READY, TIMEOUT_5S)){
				printTop("Writing failed at 0x%06X\n", writeAddr);
				writeFlash(writeAddr, 0xFF);
				return -1;
			}
			writeAddr-=0x400;
			bufAddr-=0x400;
			fails++;
		}else{
			fails = 0;
		}
		
		writeFlash(writeAddr, 0xFF);
		writeAddr+=0x400;
	}
	
	return 0;
}





int eraseSectorMSP100(int sector){
	if(sector < 0 || sector > 512) return -1;
	u32 blockAddr = sector * 0x10000;
	
	writeFlash(blockAddr, 0xF0F0);
	writeFlash(0x1554, 0xAAA9);
	writeFlash(0xAAA, 0x5556);
	writeFlash(0x1554, 0x8080);
	writeFlash(0x1554, 0xAAA9);
	writeFlash(0xAAA, 0x5556);
	writeFlash(blockAddr, 0x3030);

	waitForFlash(blockAddr, WAIT_NON_FFFF, 0x10000);

	if(waitForFlash(blockAddr, WAIT_FOR_FFFF, TIMEOUT_5S)){
		writeFlash(blockAddr, 0xF0F0);
		return 0;
	}
	
	writeFlash(blockAddr, 0xF0F0);
	return -1;
}



int flashMSP100(FILE* rom, u32 startAddr){
	int retries = 0;
	unsigned int fsz = getFileSize(rom);
	int sectors = 1+(fsz-1)/0x10000;
	int writing = 1;
	u32 addr = startAddr;
	int bufAddr = 0;
	gStatus.totalSectors = sectors;

	for(u16 i = (startAddr/0x10000);i<(startAddr/0x10000)+sectors;i++){
		
		if(eraseSectorMSP100(i)){
			printTop("Erase timed out on sector %d.\nAborting...\n", i);
			return -1;
		}
		gStatus.sectorsErased++;
	}
	rombuffer *rombuf = getBuffer();
	writeFlash(addr, 0xF0F0);
	writeFlash(0x1554, 0xAAA9);
	writeFlash(0xAAA, 0x5556);
	writeFlash(0x1554, 0x2020);

	while(writing && addr < fsz){
		gStatus.wordsFlashed = addr;
		bufAddr = 0;
		fillBuffer(rombuf, BUFFER_SIZE, rom);
		if(rombuf->size < BUFFER_SIZE){
			writing=0;
		}
		
		for(unsigned int i = 0; i<rombuf->size;i+=2){
			if(readFlash(addr) != 0xFFFF && !retries){
				printTop("Retrying erase at %06X\n",addr);
				writeFlash(addr, 0xF0F0);
				writeFlash(0x1554, 0xAAA9);
				writeFlash(0xAAA, 0x5556);
				writeFlash(0x1554, 0x8080);
				writeFlash(0x1554, 0xAAA9);
				writeFlash(0xAAA, 0x5556);
				writeFlash(addr, 0x3030);
				waitForFlash(addr, WAIT_NON_FFFF, 0x10000);
				if(!waitForFlash(addr, WAIT_FOR_FFFF, TIMEOUT_5S)){
					writeFlash(addr, 0xF0F0);
					printWriteErrorMsg(addr, 0xFFFF);
					return -1;
				}
				retries++;
			}else{
				retries = 0;
			}
			
			u16 data = rombuf->data[bufAddr] | (rombuf->data[bufAddr+1]<<8);
			writeFlash(addr, 0xA0A0);
			writeFlash(addr, data);
			
			int timeout = 0x10000;
			while(timeout && readFlash(addr) != data){
				timeout--;
			}
			if(!timeout){
				printWriteErrorMsg(addr, data);
				return -1;
			}else if(i > rombuf->size){
				break;
			}
			
			bufAddr+=2;
			addr+=2;
		}
		
		
	}
	writeFlash(addr, 0x9090);
	writeFlash(addr, 0xF0F0);
	return 0;
}





int eraseSectorMSP100Shifted(int sector){
	if(sector < 0 || sector > 512) return -1;
	u32 blockAddr = sector * 0x10000;
	
	writeFlash(blockAddr, 0xF0F0);
	writeFlash(0xAAA, 0xAAA9);
	writeFlash(0x554, 0x5556);
	writeFlash(0xAAA, 0x8080);
	writeFlash(0xAAA, 0xAAA9);
	writeFlash(0x554, 0x5556);
	writeFlash(blockAddr, 0x3030);
	
	waitForFlash(blockAddr, WAIT_NON_FFFF, 0x10000);

	if(waitForFlash(blockAddr, WAIT_FOR_FFFF, TIMEOUT_5S)){
		writeFlash(blockAddr, 0xF0F0);
		return 0;
	}
	writeFlash(blockAddr, 0xF0F0);
	return -1;
}



int flashMSP100Shifted(FILE* rom, u32 startAddr){
	int retries = 0;
	unsigned int fsz = getFileSize(rom);
	int sectors = 1+(fsz-1)/0x10000;
	int writing = 1;
	u32 addr = startAddr;
	int bufAddr = 0;
	gStatus.totalSectors = sectors;

	for(u16 i = (startAddr/0x10000);i<(startAddr/0x10000)+sectors;i++){
		
		if(eraseSectorMSP100Shifted(i)){
			printTop("Erase timed out on sector %d.\nAborting...\n", i);
			return -1;
		}
		gStatus.sectorsErased++;
	}
	rombuffer *rombuf = getBuffer();
	writeFlash(addr, 0xF0F0);
	writeFlash(0xAAA, 0xAAA9);
	writeFlash(0x554, 0x5556);
	writeFlash(0xAAA, 0x2020);

	while(writing && addr < fsz){
		gStatus.wordsFlashed = addr;
		bufAddr = 0;
		fillBuffer(rombuf, BUFFER_SIZE, rom);
		if(rombuf->size < BUFFER_SIZE){
			writing=0;
		}
		
		for(unsigned int i = 0; i<rombuf->size;i+=2){
			if(readFlash(addr) != 0xFFFF && !retries){
				printTop("Retrying erase at %06X\n",addr);
				writeFlash(addr, 0xF0F0);
				writeFlash(0xAAA, 0xAAA9);
				writeFlash(0x554, 0x5556);
				writeFlash(0xAAA, 0x8080);
				writeFlash(0xAAA, 0xAAA9);
				writeFlash(0x554, 0x5556);
				writeFlash(addr, 0x3030);
				waitForFlash(addr, WAIT_NON_FFFF, 0x10000);
				if(!waitForFlash(addr, WAIT_FOR_FFFF, TIMEOUT_5S)){
					writeFlash(addr, 0xF0F0);
					printWriteErrorMsg(addr, 0xFFFF);
					return -1;
				}
				retries++;
			}else{
				retries = 0;
			}
			
			u16 data = rombuf->data[bufAddr] | (rombuf->data[bufAddr+1]<<8);
			writeFlash(addr, 0xA0A0);
			writeFlash(addr, data);
			
			int timeout = 0x10000;
			while(timeout && readFlash(addr) != data){
				timeout--;
			}
			if(!timeout){
				printWriteErrorMsg(addr, data);
				return -1;
			}else if(i > rombuf->size){
				break;
			}
			
			bufAddr+=2;
			addr+=2;
		}
		
		
	}
	writeFlash(addr, 0xF0F0);
	return 0;
}






void resetMSP128(){
	writeFlashFlip(0xAAA, 0xA9);
	writeFlashFlip(0x554, 0x56);
	writeFlash(0x0, 0xF0);
}


int eraseSectorMSP128(int sector){
	u32 addr = 0x10000*sector;
	
	resetMSP128();
	writeFlashFlip(0xAAA, 0xA9);
	writeFlashFlip(0x554, 0x56);
	writeFlash(0xAAA, 0x80);
	writeFlashFlip(0xAAA, 0xA9);
	writeFlashFlip(0x554, 0x56);
	writeFlash(addr, 0x30);

	waitForFlash(addr, WAIT_NON_FFFF, 0x100000);
	if(!waitForFlash(addr, WAIT_FOR_FFFF, TIMEOUT_8S)){
		resetMSP128();
		return -1;
	}
	
	resetMSP128();
	return 0;
}


int flashMSP128(FILE* rom, u32 startAddr){
	unsigned int fsz = getFileSize(rom);
	if(fsz > 32*1024*1024){fsz = 32*1024*1024;}
	int sectors = 1+(fsz-1)/0x10000;
	
	gStatus.totalSectors = sectors;
	
	for(u16 i = (startAddr/0x10000);i<(startAddr/0x10000)+sectors;i++){
		
		if(eraseSectorMSP128(i) == -1){
			printTop("Erase timed out on sector %d.\nAborting...\n", i);
			return -1;
		}
		gStatus.sectorsErased++;
	}
	rombuffer *rombuf = getBuffer();
	bool writing = true;
	u32 flashAddr = startAddr;
	
	writeFlashFlip(0xAAA, 0xA9);
	writeFlashFlip(0x554, 0x56);
	writeFlash(0xAAA, 0x20);
	
	while(writing){
		gStatus.wordsFlashed = flashAddr;
		
		fillBuffer(rombuf, BUFFER_SIZE, rom);
		if(rombuf->size < BUFFER_SIZE){
			writing = false;
		}
		for(u32 bufAddr = 0;(bufAddr<rombuf->size) && (flashAddr < fsz);bufAddr+=2){
			u16 data = rombuf->data[bufAddr] | (rombuf->data[bufAddr+1]<<8);
			writeFlash(flashAddr, 0xA0);
			writeFlash(flashAddr, data);
			int timeout = 0x100000;
			while(timeout && readFlash(flashAddr) != data){
				timeout--;
			}
			if(!timeout){
				printWriteErrorMsg(flashAddr, data);
				return -1;
			}
			flashAddr+=2;
		}

	}
	resetMSP128();
	return 0;
}

void EG0xxGotoChipOffset(u32 offset){
	u32 chipAddr = (offset/32 * 0x10000000) + (0x4000C0 + (offset & 31) * 0x20202);
	union{
		u32 addr;
		u8 byte[4];
	}addr;
	u16 data = readFlash(0xBD);
	addr.addr = chipAddr;
	*(vu8*)0x0A000002 = addr.byte[3];
	*(vu8*)0x0A000003 = addr.byte[2];
	*(vu8*)0x0A000004 = addr.byte[1];
	int timeout = 0x1000;
	while(timeout && readFlash(0xBD) == data)timeout--;
}


int writeEG0xxMultiCart(FILE * f, u32 offset){
	EG0xxGotoChipOffset(offset);
	return writeFlashRom(f, detect());
}
