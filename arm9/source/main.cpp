#include <fat.h>
#include "utils.h"


#define ROM_DIR "GBA"
#define DUMP_DIR "GBA_BACKUP"
#define FRAME_DIR "frames"


void flashRom();
void dataDumper();
void injectData();

void launchSlot2();
void eraseFlash();



int main(){
	initGraphics();
	fatInitDefault();
	printTop("GBABF 1.2 - GBA Bootleg Flasher\n\n");
	
	//slow down gba slot access time for possible stability
	REG_EXMEMCNT = 0xF;
		
	while(1){
		Menu *modeSelect = new Menu;
		modeSelect->setTitle("Select A Mode");
		
		modeSelect->addOption("Flash ROM");
		modeSelect->addOption("Detect Flash");
		modeSelect->addOption("Compare cart to file");
		modeSelect->addOption("ROM Viewer");
		modeSelect->addOption("List EG0xx-Multicart Games");
		modeSelect->addOption("Dump data");
		modeSelect->addOption("Flash data");
		modeSelect->addOption("Erase flash");
		modeSelect->addOption("Launch Slot-2 Game");
		
		int mode = modeSelect->getDecision();

		switch(mode){
			case 0:
				flashRom();
			break;
	
			case 1:
				detectionTest();
			break;
	
			case 2:
				verify();
			break;
			
			case 3:
				memView();
			break;
			
			case 4:
				listMulticartGames();
			break;
			
			case 5:
				dataDumper();
			break;
			
			case 6:
				injectData();
			break;
			
			case 7:
				eraseFlash();
			break;
			
			case 8:
				launchSlot2();
			break;
		}

	delete modeSelect;
	}
	
}






void launchSlot2(){
	if(!isGame()){ //Nintendo logo is incorrect and the game won't boot
		printTop("WARNING: ROM header is invalid!\n");
	}
	
	u32 screen = PersonalData->gbaScreen;
	cBMP15 border;
	const std::string screenStrings[] = {"Screen: Top", "Screen: Bottom"};
	std::string dir = "/" FRAME_DIR;
	
	while(1){
		Menu gbaOptions("Launch Slot-2 Game");
		gbaOptions.addOption("Launch");
		gbaOptions.addOption(screenStrings[screen]);
		gbaOptions.addOption("Set Custom Border...");
		switch(gbaOptions.getDecision()){
			case 0:
				BootGbaARM9(screen, border);
			break;
			
			case 1:
				screen = (screen+1)&1;
			break;
			
			case 2:
				border = createBMP15FromFile(getFile("Select border (15bit BMP)", dir).c_str());
				if(!border.valid()){
					printTop("Selected border was invalid!\n");
				}else{
					printTop("Border set!\n");
				}
			break;
			
			
			default:
			return;
		}
	}
	
}


void dataDumper(){
	s32 dumpSrcAddr = numericalMenu("Enter dump start offset\n", 0, 0x1FF0000, 0x10000).getNumerical();
	if(dumpSrcAddr < 0){
		printTop("Dump cancelled.\n");
		return;
	}
	
	Menu sizeMenu("Enter dump size:");
	sizeMenu.addOption("64 kbytes");
	sizeMenu.addOption("128 kbytes");
	sizeMenu.addOption("4 Mbytes");
	sizeMenu.addOption("8 Mbytes");
	sizeMenu.addOption("16 Mbytes");
	sizeMenu.addOption("32 Mbytes");
	int bytes;
	
	switch(sizeMenu.getDecision()){
		case 0:
			bytes = 64*1024;
		break;
		
		case 1:
			bytes = 128*1024;
		break;
		
		case 2:
			bytes = 4*1024*1024;
		break;
		
		case 3:
			bytes = 8*1024*1024;
		break;
		
		case 4:
			bytes = 16*1024*1024;
		break;
		
		case 5:
			bytes = 32*1024*1024;
		break;
		
		default:
		printTop("Dump cancelled.\n");
		return;
	}
	
	
	char name[40] = {};
	
	if(dirExists("/" DUMP_DIR)){
		strncpy(name, DUMP_DIR "/", strlen(DUMP_DIR)+1);		
	}
	
	if(*(char*)0x80000A0){
		strncpy(&name[strlen(name)], (char*)0x80000A0, 12);
	}else{
		strcpy(&name[strlen(name)], "Untitled");
	}
	
	const int lenNoExt = strlen(name);
	strncpy(&name[lenNoExt], ".bin\0", 5);
		
	u16 nameindex = 0;
	while(fileExists(name)){
		nameindex++;
		sprintf(&name[lenNoExt], "(%d).bin",nameindex);
	}
	
	
	FILE *fp = fopen(name, "wb");
	if(!fp){
		printTop("Unable to open file\n");
		return;
	}
	
	printTop("Saving as:\n");
	printTop("%s\n", name);
	
	const int BLOCKSIZE = 64*1024;
	const int blocks = bytes/BLOCKSIZE;
	consoleClear();
	printf("\x1b[7;4H%s", name);
	
	//make some test dumps to confirm this works & fix sram backup in gamelist
	for(int i = 0;i<blocks;i++){
		printf("\x1b[2;6H=== Dumping data ===\x1b[10;4HSaving blocks(%d / %d)", i, blocks);
		fwrite((void*)(0x8000000+dumpSrcAddr), 2, BLOCKSIZE/2, fp);
		dumpSrcAddr += BLOCKSIZE;
	}
	fclose(fp);
	printTop("Dump completed!\n");
	
}






void injectData(){
	std::string dir = dirExists("/" DUMP_DIR) ? "/" DUMP_DIR : ".";
	std::string f = getFile("Choose data file to flash", dir);
	
	FILE *fp = fopen(f.c_str(), "rb");
	if(!fp){
		printTop("\nUnable to open file.\n");
		return;
	}
	
	int saveAddr = numericalMenu("Enter data destination address\n", 0, 0x1FF0000, 0x10000).getNumerical();
	if(saveAddr == -1){
		printTop("Data insertion cancelled\n");
		return;
	}
	
	startFlashingUI();
	if(writeFlashRom(fp, detect(), saveAddr)){
		printTop("Flashing failed.\n");
	}
	stopFlashingUI();
	fclose(fp);
}









void flashRom(){
	std::string dir = dirExists(ROM_DIR) ? "./" ROM_DIR : ".";
	printTop("\n\n");
	std::string f = getFile("Choose ROM to flash", dir);
	FILE *fp = fopen(f.c_str(), "rb");
	if(!fp){
		printTop("\nUnable to open file.\n");
		return;
	}
	Menu *proto = new Menu("Choose Flashing Method");
	proto->addOption("Auto-Detect");
	proto->addOption("MSP128");
	proto->addOption("MSP100");
	proto->addOption("Intel");
	proto->addOption("Intel Buffered");
	proto->addOption("MSP128 (data flip)");
	proto->addOption("MSP100 (address shift)");
	proto->addOption("Intel Buffered (data flip)");
	proto->addOption("EG0xx Multicart");
	
	cartType methods[] = {{MSP128, 0}, {MSP100, 0}, {INTEL, 0}, {INTEL_BUFFERED, 0}, {MSP128,1}, {MSP100SHIFTED,0}, {INTEL_BUFFERED,1}};
	int p = proto->getDecision();
	delete proto;
	u32 addr;
	int result;
	
	if(p < 0){
		fclose(fp);
		return;
	}
	
	switch(p){
		case 0:
			startFlashingUI();
			result = writeFlashRom(fp, detect());
		break;
		
		case 8:
		addr = askMulticartOffset();
			//B was pressed
			if(addr < 0){
				fclose(fp);
				return;
			}
			startFlashingUI();
			result = writeEG0xxMultiCart(fp, addr);
		break;
		
		default:
			startFlashingUI();
			result = writeFlashRom(fp, methods[p-1]);
		break;
		
	}
	if(result < 0){
		printTop("Flashing failed.\n\n");
	}else{
		int failedBlock = fastVerify(fp);
		if(failedBlock >= 0){
			printTop("Possible error on sector %d\n\n", failedBlock);
		}else{
			printTop("Completed in %d seconds\n\n", gStatus.seconds);
		}
	}
	stopFlashingUI();
	fclose(fp);
}



void eraseFlash(){
	Menu proto("Choose Erasing Method");
	proto.addOption("Auto-Detect");
	proto.addOption("MSP128");
	proto.addOption("MSP100");
	proto.addOption("Intel");
	proto.addOption("MSP128 (data flip)");
	proto.addOption("MSP100 (address shift)");
	
	cartType methods[] = {{MSP128, 0}, {MSP100, 0}, {INTEL, 0}, {MSP128,1}, {MSP100SHIFTED,0}};
	int p = proto.getDecision();

	if(p == -1){
		printTop("Erase cancelled!\n");
		return;
	}
	int result;
	startFlashingUI();
	if(!p){
		result = eraseFlashRom(detect());
	}else{
		result = eraseFlashRom(methods[p-1]);
	}
	stopFlashingUI();
	
	if(result != -1){
		printTop("Erase completed!\n");
	}else{
		printTop("Erase aborted!\n");
	}
	
}
