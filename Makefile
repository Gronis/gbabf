ifeq ($(strip $(DEVKITARM)),)
ifeq ($(shell which docker),)
$(error "Error: Neither DEVKITARM or docker is found. Unable to build.")
endif
endif

#---------------------------------------------------------------------------------
# We have DEVKITARM, so we build locally
#---------------------------------------------------------------------------------
ifneq ($(strip $(DEVKITARM)),)

include $(DEVKITARM)/ds_rules

all: checkarm7 checkarm9 $(TARGET).nds

clean_deps:
	$(MAKE) -C arm9 clean
	$(MAKE) -C arm7 clean

$(TARGET).nds	:	arm7/$(TARGET).elf arm9/$(TARGET).elf
	ndstool	-c $(TARGET).nds -7 arm7/$(TARGET).elf -9 arm9/$(TARGET).elf -b $(ICON) "$(GAME_TITLE);$(GAME_SUBTITLE)" > /dev/null

checkarm7 arm7/$(TARGET).elf:
	$(MAKE) -C arm7
	
checkarm9 arm9/$(TARGET).elf:
	$(MAKE) -C arm9

.PHONY: checkarm7 checkarm9 clean clean_deps all

#---------------------------------------------------------------------------------
else
#---------------------------------------------------------------------------------
# We don't have DEVKITARM, but we have docker, so we use docker to build
#---------------------------------------------------------------------------------

all: $(TARGET).nds

clean_deps:
	docker image inspect devkitpro/devkitarm 2>&1 > /dev/null && docker image rm devkitpro/devkitarm || sleep 0

$(TARGET).nds:
	docker run --rm -it -v `pwd`:/gbabf devkitpro/devkitarm bash -c "cd /gbabf && make"

.PHONY: clean clean_deps all docker

#---------------------------------------------------------------------------------
endif

clean: clean_deps
	rm -rf arm7/build
	rm -rf arm9/build
	rm -f arm7/$(TARGET).elf arm9/$(TARGET).elf
	rm -f $(TARGET).nds $(TARGET).arm7 $(TARGET).arm9

export TARGET		:=	$(shell basename $(CURDIR))
export TOPDIR		:=	$(CURDIR)

GAME_TITLE    := GBABF
GAME_SUBTITLE := Reflash your repros!
ICON := icon.bmp
